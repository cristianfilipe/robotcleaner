﻿using RobotCleaner.Core.Enumerator;
using RobotCleaner.Core.Model;
using System.Collections.Generic;

namespace RobotCleaner.Core.Util
{
    public class MapCoordinateUtil
    {
        private static Dictionary<Direction, Coordinate> _mapDirectionCoordinate = new Dictionary<Direction, Coordinate>
        {
            {Direction.North, new Coordinate(0, 1)},
            {Direction.South, new Coordinate(0, -1)},
            {Direction.East, new Coordinate(1, 0)},
            {Direction.West, new Coordinate(-1, 0)}
        };

        private static Dictionary<string, Direction> _mapDirectionName = new Dictionary<string, Direction>
        {
            {"N", Direction.North},
            {"S", Direction.South},
            {"E", Direction.East},
            {"W", Direction.West}
        };

        public static Coordinate GetDirectionStep(Direction direction)
        {
            return _mapDirectionCoordinate[direction];
        }

        public static Direction GetDirection(string directionName)
        {
            return _mapDirectionName[directionName];
        }
    }
}
