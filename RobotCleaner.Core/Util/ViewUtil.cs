﻿using RobotCleaner.Core.Interfaces;
using System;

namespace RobotCleaner.Core.Util
{
    public class ViewUtil : IViewUtil
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }

        public void WriteLine(string output)
        {
            Console.WriteLine(output);
            Console.ReadLine();
        }
    }
}
