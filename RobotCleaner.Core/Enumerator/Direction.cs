﻿namespace RobotCleaner.Core.Enumerator
{
    public enum Direction
    {
        North,
        South,
        West,
        East
    }
}
