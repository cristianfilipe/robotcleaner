﻿using RobotCleaner.Core.Model;

namespace RobotCleaner.Core.Interfaces
{
    public interface ICommandReaderService
    {
        int ReadNumberOfCommands();

        Coordinate ReadStartingCoordinate();

        RobotMovement ReadMovementCommand();

        CleanningSession ReadAllCommands();
    }
}
