﻿namespace RobotCleaner.Core.Interfaces
{
    public interface IViewUtil
    {
        string ReadLine();

        void WriteLine(string output);
    }
}
