﻿using RobotCleaner.Core.Model;

namespace RobotCleaner.Core.Interfaces
{
    public interface IRobotService
    {
        int ExecuteClean(CleanningSession session);
    }
}
