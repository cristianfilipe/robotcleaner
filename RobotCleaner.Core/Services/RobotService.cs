﻿using RobotCleaner.Core.Enumerator;
using RobotCleaner.Core.Interfaces;
using RobotCleaner.Core.Model;
using RobotCleaner.Core.Util;
using System.Collections.Generic;

namespace RobotCleaner.Core.Services
{
    public class RobotService : IRobotService
    {
        private Coordinate _currentPosition;
        private Dictionary<Coordinate, bool> _cleanOffices;

        public Coordinate CurrentPosition
        {
            get { return _currentPosition; }
            private set
            {
                _currentPosition = value;
                CleanCurrentPosition();
            }
        }

        public RobotService()
        {
            _cleanOffices = new Dictionary<Coordinate, bool>();
        }

        public void JumpTo(Coordinate position)
        {
            CurrentPosition = position;
        }

        private void CleanCurrentPosition()
        {
            _cleanOffices[_currentPosition] = true;
        }

        public void MoveTowards(Direction direction, int steps)
        {
            Coordinate directionStep = MapCoordinateUtil.GetDirectionStep(direction);
            for (int i = 0; i < steps; i++)
            {
                CurrentPosition = new Coordinate(_currentPosition.X + directionStep.X, _currentPosition.Y + directionStep.Y);
            }
        }

        public int ExecuteClean(CleanningSession session)
        {
            JumpTo(session.StartingCoordinate);
            foreach (var command in session.Commands)
            {
                this.MoveTowards(command.Direction, command.Steps);
            }
            return _cleanOffices.Count;
        }
    }
}
