﻿using RobotCleaner.Core.Interfaces;
using RobotCleaner.Core.Model;
using RobotCleaner.Core.Util;
using System.Collections.Generic;

namespace RobotCleaner.Core.Services
{
    public class CommandReaderService : ICommandReaderService
    {
        IViewUtil _view;
        public CommandReaderService(IViewUtil view)
        {
            _view = view;
        }

        public int ReadNumberOfCommands()
        {
            return int.Parse(_view.ReadLine());
        }

        public Coordinate ReadStartingCoordinate()
        {
            string line = _view.ReadLine();
            string[] coordinates = line.Split(' ');
            int x = int.Parse(coordinates[0]);
            int y = int.Parse(coordinates[1]);
            return new Coordinate(x, y);
        }

        public RobotMovement ReadMovementCommand()
        {
            string line = _view.ReadLine();
            string[] details = line.Split(' ');
            return new RobotMovement(MapCoordinateUtil.GetDirection(details[0]), int.Parse(details[1]));
        }

        public CleanningSession ReadAllCommands()
        {
            int amountOfCommands = ReadNumberOfCommands();
            Coordinate startingCoordinate = ReadStartingCoordinate();
            List<RobotMovement> commands = new List<RobotMovement>();
            while (commands.Count < amountOfCommands)
            {
                commands.Add(ReadMovementCommand());
            }
            CleanningSession session = new CleanningSession(startingCoordinate, commands);
            return session;
        }
    }
}
