﻿using System.Collections.Generic;

namespace RobotCleaner.Core.Model
{
    public class CleanningSession
    {
        public Coordinate StartingCoordinate { get; }
        public List<RobotMovement> Commands { get; }
        public CleanningSession(Coordinate startingCoordinate, List<RobotMovement> commands)
        {
            StartingCoordinate = startingCoordinate;
            Commands = commands;
        }
    }
}
