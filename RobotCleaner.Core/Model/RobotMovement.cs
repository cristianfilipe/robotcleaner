﻿using RobotCleaner.Core.Enumerator;

namespace RobotCleaner.Core.Model
{
    public class RobotMovement
    {
        public Direction Direction { get; }

        public int Steps { get; }

        public RobotMovement(Direction direction, int steps)
        {
            Direction = direction;
            Steps = steps;
        }
    }
}
