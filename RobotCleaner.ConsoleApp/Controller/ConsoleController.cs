﻿using RobotCleaner.Core.Interfaces;
using RobotCleaner.Core.Model;

namespace RobotCleaner.ConsoleApp.Controller
{
    public class ConsoleController
    {
        private readonly IViewUtil _view;
        private readonly ICommandReaderService _reader;
        private readonly IRobotService _robot;

        public ConsoleController(IViewUtil view, ICommandReaderService reader, IRobotService robot)
        {
            _view = view;
            _reader = reader;
            _robot = robot;
        }

        public void Run()
        {
            CleanningSession session = _reader.ReadAllCommands();
            int placesClean = _robot.ExecuteClean(session);
            _view.WriteLine($"=> Cleaned: {placesClean}");
        }
    }
}
