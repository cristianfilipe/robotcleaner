﻿using RobotCleaner.ConsoleApp.Controller;
using RobotCleaner.Core.Interfaces;
using RobotCleaner.Core.Services;
using RobotCleaner.Core.Util;

namespace RobotCleaner.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IViewUtil view = new ViewUtil();
            ICommandReaderService reader = new CommandReaderService(view);
            IRobotService robot = new RobotService();
            ConsoleController controller = new ConsoleController(view, reader, robot);
            controller.Run();
        }
    }
}
