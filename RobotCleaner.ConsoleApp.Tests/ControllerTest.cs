﻿using Moq;
using RobotCleaner.ConsoleApp.Controller;
using RobotCleaner.Core.Interfaces;
using RobotCleaner.Core.Model;
using System.Collections.Generic;
using Xunit;

namespace RobotCleaner.ConsoleApp.Tests
{
    public class ControllerTest
    {
        Mock<IViewUtil> _mockView;
        Mock<ICommandReaderService> _mockReader;
        Mock<IRobotService> _mockRobot;
        ConsoleController _consoleController;

        public ControllerTest()
        {
            _mockView = new Mock<IViewUtil>();
            _mockReader = new Mock<ICommandReaderService>();
            _mockRobot = new Mock<IRobotService>();
            _consoleController = new ConsoleController(_mockView.Object, _mockReader.Object, _mockRobot.Object);
        }

        [Fact]
        public void Run_should_display_places_cleaned()
        {
            _mockReader.Setup(x => x.ReadAllCommands())
                       .Returns(new CleanningSession(new Coordinate(0, 0), new List<RobotMovement>()));

            _mockRobot.Setup(x => x.ExecuteClean(It.IsAny<CleanningSession>())).Returns(1001);
            _mockView.Setup(x => x.WriteLine(It.IsAny<string>()));

            _consoleController.Run();

            _mockView.Verify(w => w.WriteLine(It.Is<string>(s => s == "=> Cleaned: 1001")), Times.Once);
        }

    }
}
