﻿using RobotCleaner.Core.Enumerator;
using RobotCleaner.Core.Model;
using RobotCleaner.Core.Services;
using System.Collections.Generic;
using Xunit;

namespace RobotCleaner.Core.Tests
{
    public class RobotServiceTest
    {
        RobotService _robotService;
        public RobotServiceTest()
        {
            _robotService = new RobotService();
        }

        [Fact]
        public void JumpTo_position_should_change_CurrentPosition()
        {
            _robotService.JumpTo(new Coordinate(-2000, 3000));

            Assert.Equal(-2000, _robotService.CurrentPosition.X);
            Assert.Equal(3000, _robotService.CurrentPosition.Y);
        }

        [Fact]
        public void MoveTowards_direction_E_should_change_CurrentPosition()
        {
            _robotService.JumpTo(new Coordinate(0, 0));

            _robotService.MoveTowards(Direction.East, 2000);

            Assert.Equal(2000, _robotService.CurrentPosition.X);
            Assert.Equal(0, _robotService.CurrentPosition.Y);
        }

        [Fact]
        public void MoveTowards_direction_W_should_change_CurrentPosition()
        {
            _robotService.JumpTo(new Coordinate(0, 0));
            _robotService.MoveTowards(Direction.West, 2000);

            Assert.Equal(-2000, _robotService.CurrentPosition.X);
            Assert.Equal(0, _robotService.CurrentPosition.Y);
        }

        [Fact]
        public void MoveTowards_direction_N_should_change_CurrentPosition()
        {
            _robotService.JumpTo(new Coordinate(0, 0));
            _robotService.MoveTowards(Direction.North, 2000);

            Assert.Equal(0, _robotService.CurrentPosition.X);
            Assert.Equal(2000, _robotService.CurrentPosition.Y);
        }

        [Fact]
        public void MoveTowards_direction_S_should_change_CurrentPosition()
        {
            _robotService.JumpTo(new Coordinate(0, 0));
            _robotService.MoveTowards(Direction.South, 2000);

            Assert.Equal(0, _robotService.CurrentPosition.X);
            Assert.Equal(-2000, _robotService.CurrentPosition.Y);
        }

        [Fact]
        public void ExecuteClean_Session_One_Line_should_return_4001()
        {
            List<RobotMovement> commands = new List<RobotMovement>();
            commands.Add(new RobotMovement(Direction.East, 4000));
            CleanningSession session = new CleanningSession(new Coordinate(-2000, 2000), commands);

            int places = _robotService.ExecuteClean(session);

            Assert.Equal(2000, _robotService.CurrentPosition.X);
            Assert.Equal(2000, _robotService.CurrentPosition.Y);
            Assert.Equal(4001, places);
        }

        [Fact]
        public void ExecuteClean_Session_Square_should_return_800000()
        {
            List<RobotMovement> commands = new List<RobotMovement>();
            commands.Add(new RobotMovement(Direction.East, 200000));
            commands.Add(new RobotMovement(Direction.South, 200000));
            commands.Add(new RobotMovement(Direction.West, 200000));
            commands.Add(new RobotMovement(Direction.North, 200000));
            CleanningSession session = new CleanningSession(new Coordinate(-100000, 100000), commands);
            int places = _robotService.ExecuteClean(session);

            Assert.Equal(-100000, _robotService.CurrentPosition.X);
            Assert.Equal(100000, _robotService.CurrentPosition.Y);
            Assert.Equal(800000, places);
        }

        [Fact]
        public void ExecuteClean_Session_Repeating_line_should_return_3001()
        {
            List<RobotMovement> commands = new List<RobotMovement>();
            commands.Add(new RobotMovement(Direction.South, 3000));
            commands.Add(new RobotMovement(Direction.North, 3000));
            CleanningSession session = new CleanningSession(new Coordinate(-20000, -20000), commands);
            int places = _robotService.ExecuteClean(session);

            Assert.Equal(-20000, _robotService.CurrentPosition.X);
            Assert.Equal(-20000, _robotService.CurrentPosition.Y);
            Assert.Equal(3001, places);
        }

        [Fact]
        public void ExecuteClean_Session_3_Intersections_should_avoid_repeated()
        {
            List<RobotMovement> commands = new List<RobotMovement>();
            commands.Add(new RobotMovement(Direction.East, 100));
            commands.Add(new RobotMovement(Direction.South, 20));
            commands.Add(new RobotMovement(Direction.West, 20));
            commands.Add(new RobotMovement(Direction.North, 24));
            commands.Add(new RobotMovement(Direction.East, 10));
            commands.Add(new RobotMovement(Direction.South, 30));
            CleanningSession session = new CleanningSession(new Coordinate(0, 0), commands);
            int places = _robotService.ExecuteClean(session);

            Assert.Equal(100 - 20 + 10, _robotService.CurrentPosition.X);
            Assert.Equal(-20 + 24 - 30, _robotService.CurrentPosition.Y);
            Assert.Equal(1 + 100 + 20 + 20 + 24 + 10 + 30 - 3, places);
        }
    }
}
