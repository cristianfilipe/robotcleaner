using Moq;
using RobotCleaner.Core.Enumerator;
using RobotCleaner.Core.Interfaces;
using RobotCleaner.Core.Model;
using RobotCleaner.Core.Services;
using Xunit;

namespace RobotCleaner.Core.Tests
{

    public class CommandReaderTest
    {
        Mock<IViewUtil> _mockView;
        CommandReaderService _commandReaderService;

        public CommandReaderTest()
        {
            _mockView = new Mock<IViewUtil>();
            _commandReaderService = new CommandReaderService(_mockView.Object);
        }

        [Fact]
        public void ReadNumberOfCommands_integer_input_should_return_integer()
        {
            _mockView.Setup(x => x.ReadLine()).Returns("44");
            int result = _commandReaderService.ReadNumberOfCommands();

            Assert.Equal(44, result);
        }

        [Fact]
        public void ReadStartingCoordinate_pair_integer_input_should_return_Coordinate()
        {
            _mockView.Setup(x => x.ReadLine()).Returns("2000 3000");
            Coordinate result = _commandReaderService.ReadStartingCoordinate();

            Assert.Equal(2000, result.X);
            Assert.Equal(3000, result.Y);
        }

        [Fact]
        public void ReadCommand_text_input_should_return_MoveCommand()
        {
            _mockView.Setup(x => x.ReadLine()).Returns("N 333");
            RobotMovement result = _commandReaderService.ReadMovementCommand();

            Assert.Equal(Direction.North, result.Direction);
            Assert.Equal(333, result.Steps);
        }

        [Fact]
        public void ReadAllCommand_text_sequence_input_should_return_CleanningSession()
        {
            _mockView.SetupSequence(x => x.ReadLine()).Returns("4")
                .Returns("-100 -101")
                .Returns("N 100")
                .Returns("E 1000")
                .Returns("S 500")
                .Returns("W 10");
            CleanningSession result = _commandReaderService.ReadAllCommands();

            Assert.Equal(-100, result.StartingCoordinate.X);
            Assert.Equal(-101, result.StartingCoordinate.Y);
            Assert.Equal(4, result.Commands.Count);

            Assert.Equal(Direction.North, result.Commands[0].Direction);
            Assert.Equal(100, result.Commands[0].Steps);

            Assert.Equal(Direction.East, result.Commands[1].Direction);
            Assert.Equal(1000, result.Commands[1].Steps);

            Assert.Equal(Direction.South, result.Commands[2].Direction);
            Assert.Equal(500, result.Commands[2].Steps);

            Assert.Equal(Direction.West, result.Commands[3].Direction);
            Assert.Equal(10, result.Commands[3].Steps);
        }
    }
}
