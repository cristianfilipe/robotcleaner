﻿using RobotCleaner.Core.Model;
using Xunit;

namespace RobotCleaner.Core.Tests
{
    public class CoordinateTest
    {
        [Fact]
        public void Equals_different_instances_same_parameters_should_be_true()
        {
            Coordinate coordinate = new Coordinate(10, 15);
            Coordinate coordinate2 = new Coordinate(10, 15);
            bool result = coordinate.Equals(coordinate2);
            Assert.True(result);
        }

        [Fact]
        public void Equals_different_parameters_should_be_false()
        {
            Coordinate coordinate = new Coordinate(10, 15);
            Coordinate coordinate2 = new Coordinate(100, 150);
            bool result = coordinate.Equals(coordinate2);
            Assert.False(result);
        }

        [Fact]
        public void Equals_null_should_be_false()
        {
            Coordinate coordinate = new Coordinate(10, 15);
            bool result = coordinate.Equals(null);
            Assert.False(result);
        }

    }
}
