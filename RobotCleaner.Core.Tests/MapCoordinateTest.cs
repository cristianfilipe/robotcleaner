﻿using RobotCleaner.Core.Enumerator;
using RobotCleaner.Core.Model;
using RobotCleaner.Core.Util;
using Xunit;

namespace RobotCleaner.Core.Tests
{
    public class MapCoordinateTest
    {

        [Fact]
        public void GetDirectionStep_North_should_be_0_1()
        {
            Coordinate result = MapCoordinateUtil.GetDirectionStep(Direction.North);

            Assert.Equal(0, result.X);
            Assert.Equal(1, result.Y);
        }

        [Fact]
        public void GetDirectionStep_South_Should_Be_0_minus1()
        {
            Coordinate result = MapCoordinateUtil.GetDirectionStep(Direction.South);
            Assert.Equal(0, result.X);
            Assert.Equal(-1, result.Y);
        }

        [Fact]
        public void GetDirectionStep_East_Should_Be_1_0()
        {
            Coordinate result = MapCoordinateUtil.GetDirectionStep(Direction.East);
            Assert.Equal(1, result.X);
            Assert.Equal(0, result.Y);
        }

        [Fact]
        public void GetDirectionStep_West_Should_Be_minus1_0()
        {
            Coordinate result = MapCoordinateUtil.GetDirectionStep(Direction.West);
            Assert.Equal(-1, result.X);
            Assert.Equal(0, result.Y);
        }
    }
}
